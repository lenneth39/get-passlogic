# coding: UTF-8
import requests
import urllib.request, urllib.error
from bs4 import BeautifulSoup
import lxml.html
import numpy as np

r = urllib.request.urlopen("https://d-ras-w.ddreams.jp/passlogic/ui/keyreq.php?id=2694868%40ntt-west.local&kn=9002")
data = r.read().decode('utf-8')

soup = BeautifulSoup(data, 'html.parser')
a2 = soup.find_all('td')

result =  []
bs = np.array([])
count = 0
dammy = ""

for e in a2 :
    if count < 48:
        result.append(e.getText())
        count += 1
else:
    arr = np.array(result)
    #自分で持っているpasslogicの並び
    bs = [arr[0],arr[1],arr[2],arr[3],arr[4],arr[5],arr[6],arr[7]]
    passwd = ','.join(bs)
    print(passwd.replace(",", ""))
