# PasslogicからOTPをポチで取得

## 機能
PasslogicからOTPを取得し、クリップボードに保存

## 動作検証済み
- Windows 10
- Python 3.9.0

## 初期設定

1. ```init.bat```を実行（PIPで必要なパッケージを取得)
1. ```get-passlogic.py```の8行目ユーザ名を変更する

## パスワード取得
1. ```passlogic.bat```を実行


